#!/usr/bin/env python

import sys
import subprocess
import click
from path import Path
import yaml
import platform
import os
import sh
from sh.contrib import git
import logging

packages = [
    "build-essential", "qt5-default", "libxcb-xinerama0-dev", "^libxcb.*-dev",
    "libx11-xcb-dev", "libglu1-mesa-dev", "libxrender-dev", "libxi-dev",
    "libssl-dev", "libxcursor-dev", "libxcomposite-dev", "libxdamage-dev",
    "libxrandr-dev", "libdbus-1-dev", "libfontconfig1-dev", "libcap-dev",
    "libxtst-dev", "libpulse-dev", "libudev-dev", "libpci-dev", "libnss3-dev",
    "libasound2-dev", "libxss-dev", "libegl1-mesa-dev", "gperf", "bison",
    "libwayland-dev", "libwayland-egl1-mesa", "libwayland-server0",
    "libgles2-mesa-dev", "libxkbcommon-dev", "cmake", "zlib1g-dev",
    "libdbus-glib-1-dev", "flex", "python3-venv",
]

logging.basicConfig(level=logging.INFO, filename='qauto.log', filemode='w', format='%(message)s')
click.secho('log information in qauto.log', fg='green')


def echo_raw(msg):
    click.secho(msg, fg='blue')


def error_raw(msg):
    click.secho(msg, fg='red')


def echo_header(title):
    click.echo()
    click.secho(40 * '#', fg='green')
    click.secho('\t{0}'.format(title))
    click.secho(40 * '#', fg='green')


def echo_dot(msg):
    msg = msg.strip()
    logging.debug(msg)
    click.secho('.', fg='blue', nl=False)


def echo(msg):
    msg = msg.strip()
    logging.info(msg)
    click.secho(msg, fg='blue')


def error(msg):
    if msg.startswith('Project MESSAGE:'):
        return
    msg = msg.strip()
    logging.warn(msg)
    click.secho(msg, fg='red')


env = os.environ.copy()
env['GIT_SSL_NO_VERIFY'] = True
git = git.bake(_out=echo, _err=echo)
make = sh.Command('make').bake(_out=echo_dot, _err=error)
perl = sh.Command('perl').bake(_out=echo)


repos = [
    ['dlt-daemon', 'git://github.com/GENIVI/dlt-daemon.git', 'master'],
    ['qtapplicationmanager', 'git://code.qt.io/qt/qtapplicationmanager.git', '5.9'],
    ['qtivi', 'git://code.qt.io/qt/qtivi.git', 'dev'],
    ['neptune3-ui', 'git://code.qt.io/qt-apps/neptune3-ui.git', '5.10'],
    ['neptune-ui', 'git://code.qt.io/qt-apps/neptune-ui.git', '5.9'],
    ['gammaray', 'git://github.com/KDAB/GammaRay.git', 'master'],
    ['qmllive', 'git://code.qt.io/qt-apps/qmllive.git', '5.8'],
]

env = os.environ.copy()


is_linux = platform.system() == 'Linux'
is_macos = platform.system() == 'Darwin'


class Options:
    def __init__(self):
        self.qt_version = '5.10'
        self.source = Path('source').expand().abspath()
        self.build = Path('build').expand().abspath()
        self.install = Path('install').expand().abspath()
        self.make_jobs = 4
        self._compute()
        self.dry_run = False

    @staticmethod
    def from_obj(data):
        obj = Options()
        obj.qt_version = data.get('qt_version', obj.qt_version)
        obj.source = Path(data.get('source', obj.source))
        obj.build = Path(data.get('build', obj.build))
        obj.install = Path(data.get('install', obj.install))
        obj.make_jobs = data.get('make_jobs', obj.make_jobs)
        obj._compute()
        return obj

    def to_obj(self):
        return {
            'qt_version': self.qt_version,
            'source': str(self.source.abspath()),
            'build': str(self.build.abspath()),
            'install': str(self.install.abspath()),
            'make_jobs': self.make_jobs,
        }

    def _compute(self):
        self.qt_id = 'qt{0}'.format(self.qt_version.replace('.', ''))
        self.qt_build = self.build / self.qt_id
        self.qt_source = self.source / 'qt5'
        self.qt_install = self.install / self.qt_id

    def to_yaml(self):
        return yaml.dump(self.__dict__)

    @staticmethod
    def from_yaml(data):
        values = yaml.safe_load(data)
        return Options.from_obj(values)


options = Options()

pass_options = click.make_pass_decorator(Options, ensure=True)


DRY_RUN = False


def run(cmd, path=Path('.')):
    logging.info('run {0} in '.format(cmd, path))
    if DRY_RUN:
        click.echo('$ {0}'.format(cmd))
    else:
        with path:
            click.echo('$ {0}'.format(cmd))
            return subprocess.call(cmd, shell=True)


def load_options():
    global options
    path = Path('qauto.yml')
    if path.exists():
        data = path.text()
        obj = yaml.load(data)
        options = Options.from_obj(obj)


def clone_repo(path, url, branch):
    path = options.source / path
    with path.dirname():
        if not path.exists():
            click.echo('clone {0}'.format(url))
            git.clone(url, path.name, _env=env)
        with path:
            click.echo('update {0}'.format(url))
            git.checkout(branch)
            git.reset(hard=True)
            git.pull(_out=echo)
            git.submodule('init')
            git.submodule('update')


def qmake_build(name, rebuild=False, pause=False, qt_install=False):
    qmake = sh.Command(options.qt_install / 'bin/qmake').bake(_out=echo, _err=error)
    echo_header('build {0}'.format(name))
    source = options.source / name
    build = options.build / name
    if rebuild:
        click.secho('rm {0}'.format(build), fg='red')
        build.rmtree_p()
    build.makedirs_p()
    install = options.install if not qt_install else options.qt_install
    with build:
        qmake(source, 'INSTALL_PREFIX={0}'.format(install))
        if pause:
            click.pause()
        make(jobs=options.make_jobs)
        make.install()


def cmake_build(name, rebuild=False, pause=False):
    echo_header('build {0}'.format(name))
    cmake = sh.Command('cmake').bake(
        '-DCMAKE_PREFIX_PATH={0}/lib/cmake'.format(options.qt_install),
        _out=echo
    )

    source = options.source / name
    build = options.build / name
    install = options.install / name
    if rebuild:
        click.secho('rm {0}'.format(build), fg='red')
        build.rmtree_p()
    if not build.exists():
        build.makedirs_p()
        with build:
            cmake(source, '-DCMAKE_INSTALL_PREFIX={0}'.format(install))
            if pause:
                click.pause()
    with build:
        make(jobs=options.make_jobs)
        make.install()


@click.group(chain=True)
@click.option('--dry-run/--no-dry-run', default=False, help="default is no-dry-run")
@click.pass_context
def cli(ctx, dry_run):
    global DRY_RUN
    DRY_RUN = dry_run
    ctx.obj.dry_run = dry_run
    ctx.obj.source.makedirs_p()
    ctx.obj.build.makedirs_p()
    ctx.obj.install.makedirs_p()


@cli.command()
@click.pass_context
def setup(ctx):
    """configures the options used for qauto script"""
    obj = options.to_obj()
    for key in obj:
        obj[key] = click.prompt(key, default=obj[key])
    options.from_obj(obj)
    data = yaml.dump(obj, default_flow_style=False)
    Path('qauto.yml').write_text(data)


@cli.command()
@pass_options
def clean(options):
    """cleans the build and install folder of qtas"""
    for name, repo, branch in repos:
        path = options.build / name
        echo('rm {0}'.format(path))
        path.rmtree_p()
        path = options.install / name
        echo('rm {0}'.format(path))
        path.rmtree_p()


@cli.command()
@pass_options
def kill(options):
    """kill all instances of neptune3"""
    killall = sh.Command('killall')
    killall('neptune3-ui')


@cli.command()
@pass_options
def qt5_clean(options):
    """cleans the build and install folderof qt5"""
    options.qt_build.rmtree_p()
    options.qt_install.rmtree_p()


@cli.command()
@pass_options
def qt5_init(options):
    """clone the qt repo and initializes the sub modules"""
    if not options.qt_source.exists():
        with options.source:
            git.clone('git://code.qt.io/qt/qt5.git', branch=options.qt_version)
        with options.qt_source:
            perl('init-repository')
    with options.qt_source:
        git.pull()
        git.checkout(options.qt_version)
        git.submodule('update')


@cli.command()
@pass_options
@click.option('--minimal/--no-minimal', default=True, help="creates a minimal build. Default is minimal.")
@click.option('--release/--no-release', default=False, help="creates a release build. Default is debug-build")
def qt5_config(options, minimal, release):
    """configures qt5 from sources"""
    qt_source = options.qt_source
    qt_install = options.qt_install
    options.qt_build.makedirs_p()
    config = [
        '-opensource',
        '-confirm-license',
        '-nomake examples',
        '-nomake tests'
    ]
    if release:
        config.append('-release')
    else:
        config.append('-debug')
    if minimal:
        config.append('-skip qtandroidextras')
        config.append('-skip qtdatavis3d')
        config.append('-skip qtcharts')
        config.append('-skip qtdocgallery')
        config.append('-skip qtenginio')
        config.append('-skip qtfeedback')
        config.append('-skip qtpim')
        config.append('-skip qtpurchasing')
        config.append('-skip qtquick1')
        config.append('-skip qtscript')
        config.append('-skip qtspeech')
        config.append('-skip qtwebchannel')
        config.append('-skip qtwebengine')
        config.append('-skip qtwebglplugin')
        config.append('-skip qtwebview')
    with options.qt_build:
        if is_linux:
            config.append('-opengl es2')
        if is_macos:
            config.append('-no-framework')
        config = ' '.join(config)
        run('{0}/configure {1} -prefix {2}'.format(qt_source, config, qt_install))


@cli.command()
@click.option('--rebuild/--no-rebuild', default=False, help="rebuilds qt5 by purging the build folder. Default is no-rebuild")
@pass_options
def qt5_build(options, rebuild):
    """build qt5 from source in the build folder"""
    if rebuild:
        echo('rm {0}'.format(options.qt_build))
        options.qt_build.rmtree_p()
        echo('rm {0}'.format(options.qt_install))
        options.qt_install.rmtree_p()
    options.qt_build.makedirs_p()
    options.qt_install.makedirs_p()
    with options.qt_build:
        make(jobs=options.make_jobs)
        make.install()


@cli.command()
@pass_options
def init(options):
    """clones the qtas components"""
    for repo in repos:
        clone_repo(*repo)


@cli.command()
@click.option('--rebuild/--no-rebuild', default=False, help="rebuild by puring the build folder. Default is no-rebuild")
@click.option('--pause/--no-pause', default=False, help="pauses after the initial qmake config. Default is no-pause")
@click.option('--bare/--no-bare', default=True)
@pass_options
def build(options, rebuild, pause, bare):
    """builds all qtas components from sources"""
    if is_linux:
        cmake_build('dlt-daemon', rebuild, pause=False)
    qmake_build('qtapplicationmanager', rebuild, pause, qt_install=True)
    qmake_build('qtivi', rebuild, pause)
    qmake_build('neptune3-ui', rebuild, pause=False)
    if not bare:
        qmake_build('neptune-ui', rebuild, pause=False)
        qmake_build('qmllive', rebuild, pause=False)
        cmake_build('gammaray', rebuild, pause)


@cli.command()
@click.pass_context
def install_fonts(ctx):
    """download some fonts from the network"""
    source = ctx.obj.source
    qt_install = ctx.obj.qt_install
    with source:
        run('git clone')
    (qt_install / 'lib/fonts').makedirs_p()
    with Path('source/fonts'):
        run('googlefonts-installer -f {0}/lib/fonts')


@cli.command()
def ubuntu_init():
    """install relevant ubuntu packages for Qt5. Only required on ubuntu"""
    for package in packages:
        run("sudo apt install -qqq -y {0}".format(package))
    run("curl -s https://packagecloud.io/install/repositories/github/git-lfs/script.deb.sh | sudo bash")
    run("git lfs install")


@cli.command()
@click.pass_context
def gammaray(ctx):
    """launches gammaray"""
    install = ctx.obj.install
    click.launch('{0}/gammaray/GammaRay.app'.format(install))


@cli.command()
@click.pass_context
def appman(ctx):
    """launches the application manager"""
    qt_install = ctx.obj.qt_install


@cli.command()
@click.option('zoom', '--zoom', default='1.0', help='default is zoom=1.0')
@click.option('style', '--style', default='styles/neptune', help='default is neptune style')
@click.argument('config', type=click.Path(), default="am-config.yaml")
@click.pass_context
def neptune(ctx, zoom, style, config):
    """launches the neptune ui"""
    env['QT_SCALE_FACTOR'] = zoom
    install = ctx.obj.install / 'neptune-ui'
    with install:
        neptune = sh.Command('./neptune-ui')
        neptune('-r', config_file=config, style=style, _env=env)


@cli.command()
@click.option('zoom', '--zoom', default='1.0', help='default is zoom=1.0')
@click.argument('config', type=click.Path(), default="am-config.yaml")
@click.pass_context
def neptune3(ctx, zoom, config):
    """launches the neptune3-ui"""
    env['QT_SCALE_FACTOR'] = zoom
    install = ctx.obj.install / 'neptune3'
    with install:
        neptune3 = sh.Command('./neptune3-ui')
        if is_macos:
            env['DYLD_LIBRARY_PATH'] = install / 'lib'
            neptune3('-r', config_file=config, _env=env)
        if is_linux:
            neptune3('-r', '--start-session-dbus', config_file=config, _env=env)


@cli.command()
@click.pass_context
def server(ctx):
    """launches the servers for remote communication"""
    install = ctx.obj.install / 'neptune3'
    if is_macos:
        env['DYLD_LIBRARY_PATH'] = install / 'lib'
    with install:
        server = sh.Command('./RemoteSettings_server')
        server(_env=env)


@cli.command()
@click.pass_context
def update(ctx):
    """updates qtas repositories"""
    for repo in repos:
        clone_repo(*repo)


@cli.command()
@click.option('--follow/--no-follow', default=True)
def log(follow):
    tail = sh.tail()
    if follow:
        tail('-F', 'qauto.log')
    else:
        tail('qauto.log')


def main():
    try:
        load_options()
        cli(obj=options)
    except sh.ErrorReturnCode as err:
        error('error calling: {0}'.format(err.full_cmd))
        sys.exit(err.exit_code)


if __name__ == "__main__":
    main()
