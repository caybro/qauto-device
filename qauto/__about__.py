import os.path

here = os.path.dirname(os.path.abspath(__file__))


__title__ = "qauto-device"
__summary__ = "Qt Auto Device Controller"
__url__ = "https://gitlab.com/jryannel/qauto-device"
__version__ = "0.0.1"
__author__ = "JRyannel"
__author_email__ = "jbocklage-ryannel@luxoft.com"
__copyright__ = "2017 Luxoft"
