import os.path
from setuptools import setup, find_packages
from qauto import __about__ as about

here = os.path.dirname(os.path.abspath(__file__))
readme = open(os.path.join(here, 'README.md'), 'r').read()

setup(
    name=about.__title__,
    version=about.__version__,
    description=readme,
    long_description=readme,
    url=about.__url__,
    author=about.__author__,
    author_email=about.__author_email__,
    license='MIT',
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Intended Audience :: Developers',
        'Topic :: Software Development :: Code Generators',
        'License :: OSI Approved :: MIT License',
        'Programming Language :: Python :: 3',
    ],
    keywords='qt auto',
    packages=find_packages(),
    include_package_data=True,
    install_requires=[
        'click',
        'PyYAML',
        'path.py',
        'sh',
        'fabric',
    ],
    entry_points={
        'console_scripts': [
            'qauto-remote = qauto.remote:main',
            'qauto = qauto.manage:main'
        ],
    },
)
